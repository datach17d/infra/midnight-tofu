package main

import (
	"flag"
	"fmt"
	"os"

	"gitlab.com/datach17d/infra/midnight-tofu/pkg/output/diff"
	"gitlab.com/datach17d/infra/midnight-tofu/pkg/sources/file"
	"gitlab.com/datach17d/infra/midnight-tofu/pkg/utils"
	"gitlab.com/datach17d/infra/midnight-tofu/pkg/yaml"
)

func main() {

	flag.Parse()

	fileList := flag.Args()

	if len(fileList) == 2 {
		Af, err := os.Open(fileList[0])
		defer Af.Close()
		if err != nil {
			fmt.Fprintf(os.Stderr, "Error: %v\n", err)
			return
		}
		Bf, err := os.Open(fileList[1])
		defer Bf.Close()
		if err != nil {
			fmt.Fprintf(os.Stderr, "Error: %v\n", err)
			return
		}

		An, err := file.NewYamlDocumentNodesFromReader(Af)
		if err != nil {
			fmt.Fprintf(os.Stderr, "Error: %v\n", err)
			return
		}

		Bn, err := file.NewYamlDocumentNodesFromReader(Bf)
		if err != nil {
			fmt.Fprintf(os.Stderr, "Error: %v\n", err)
			return
		}

        utils.LineUpNodes(An.Nodes, Bn.Nodes)

		nd, err := yaml.DiffDocumentNodes(An, Bn)
		if err != nil {
			fmt.Fprintf(os.Stderr, "Error: %v\n", err)
			return
		}

		fmt.Printf("%s\n", diff.GenerateDiff(nd))
	}

}
