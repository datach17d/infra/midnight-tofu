package utils

import (
	"fmt"
	"reflect"
	"sort"
	"strings"

	yamlv3 "gopkg.in/yaml.v3"
)

type k8sType struct {
	APIVersion string `yaml:"apiVersion"`
	Kind       string `yaml:"kind"`
	Metadata   struct {
		Name      string `yaml:"name"`
		Namespace string `yaml:"namespace"`
	}
}

func (k *k8sType) String() string {
	if k.Metadata.Namespace == "" {
		return fmt.Sprintf("%s.%s.%s", k.APIVersion, k.Kind, k.Metadata.Name)
	} else {
		return fmt.Sprintf("%s.%s.%s.%s", k.APIVersion, k.Kind, k.Metadata.Namespace, k.Metadata.Name)
	}
}

func get_k8stype(n *yamlv3.Node) (result *k8sType) {

	if n != nil {
		result = &k8sType{}
		err := n.Decode(result)
		if err != nil {
			result = nil
		}
	}

	return
}

func CompareKubernetesNodes(An, Bn *yamlv3.Node) (result bool) {
	result = false

	if An != nil && Bn != nil && An.Kind == yamlv3.DocumentNode && Bn.Kind == yamlv3.DocumentNode {
		At := get_k8stype(An)
		Bt := get_k8stype(Bn)
		result = reflect.DeepEqual(At, Bt)
	}

	return
}

func SortYamlNodes(An *[]*yamlv3.Node) {
	sort.SliceStable(*An, func(i, j int) (result bool) {
		result = false
		it := get_k8stype((*An)[i])
		jt := get_k8stype((*An)[j])

		if it != nil && jt != nil {
			result = strings.Compare(it.String(), jt.String()) < 0
		}

		return
	})
}

func get_index_from_k8stype(nodes *[]*yamlv3.Node, name string) int {
	for i, node := range *nodes {
		k := get_k8stype(node)
		if k != nil {
			if k.String() == name {
				return i
			}
		}
	}
	return -1
}

func insert_nil_at_index(An *[]*yamlv3.Node, index int) {
	if index >= len(*An) {
		newValues := make([]*yamlv3.Node, len(*An)-(index-1))
		(*An) = append((*An), newValues...)
	} else {
		(*An) = append((*An)[:index+1], (*An)[index:]...)
		(*An)[index] = nil
	}
}

func LineUpNodes(An, Bn *[]*yamlv3.Node) {

	k8sItemMap := make(map[string]bool)

	for _, item := range *An {
		t := get_k8stype(item)
		if t != nil {
			k8sItemMap[t.String()] = true
		}
	}

	for _, item := range *Bn {
		t := get_k8stype(item)
		if t != nil {
			k8sItemMap[t.String()] = true
		}
	}

	k8sItemList := make([]string, len(k8sItemMap))
	i := 0
	for k := range k8sItemMap {
		k8sItemList[i] = k
		i++
	}

	sort.SliceStable(k8sItemList, func(i, j int) bool {
		return strings.Compare(k8sItemList[i], k8sItemList[j]) < 0
	})

	for _, itemName := range k8sItemList {
		Ai := get_index_from_k8stype(An, itemName)
		Bi := get_index_from_k8stype(Bn, itemName)

		if Ai == -1 && Bi >= 0 {
			insert_nil_at_index(An, Bi)
		}

		if Bi == -1 && Ai >= 0 {
			insert_nil_at_index(Bn, Ai)
		}
	}
}

func OrganiseKubernetesNodes(An, Bn *[]*yamlv3.Node) {
	SortYamlNodes(An)
	SortYamlNodes(Bn)
	LineUpNodes(An, Bn)
}
