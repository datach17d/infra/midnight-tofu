package yaml

import (
	"testing"

	"github.com/stretchr/testify/assert"

	yamlv3 "gopkg.in/yaml.v3"
)

func TestMarsahl(t *testing.T) {
	var a map[string]interface{} = map[string]interface{}{
		"test": "test",
	}

	m := yamlv3.Node{}

	m.Encode(a)

}

func TestDiffSimpleMap(t *testing.T) {
	assert := assert.New(t)

	simpleMapA := yamlv3.Node{
		Kind: yamlv3.DocumentNode,
		Content: []*yamlv3.Node{
			{
				Kind: yamlv3.MappingNode,
				Tag:  "!!map",
				Content: []*yamlv3.Node{
					{
						Kind:  yamlv3.ScalarNode,
						Tag:   "!!str",
						Value: "test",
					},
					{
						Kind:  yamlv3.ScalarNode,
						Tag:   "!!str",
						Value: "test",
					},
				},
			},
		},
	}

	simpleMapB := yamlv3.Node{
		Kind: yamlv3.DocumentNode,
		Content: []*yamlv3.Node{
			{
				Kind: yamlv3.MappingNode,
				Tag:  "!!map",
				Content: []*yamlv3.Node{
					{
						Kind:  yamlv3.ScalarNode,
						Tag:   "!!str",
						Value: "test",
					},
					{
						Kind:  yamlv3.ScalarNode,
						Tag:   "!!str",
						Value: "notest",
					},
				},
			},
		},
	}

	result := NodeDiff{
		A: &YamlDocumentNodes{
			Nodes: &[]*yamlv3.Node{&simpleMapA},
		},
		B: &YamlDocumentNodes{
			Nodes: &[]*yamlv3.Node{&simpleMapB},
		},
	}

	err := diff_document_nodes(0, &result)
	assert.NoError(err)
	assert.Len(result.Ad, len(result.Bd))
	assert.Len(result.Bd, 1)

	assert.Equal(simpleMapA.Content[0].Content[1], result.Ad[0].GetNode())
	assert.Equal(simpleMapB.Content[0].Content[1], result.Bd[0].GetNode())
}
