package yaml

import (
	"fmt"
	"reflect"

	"github.com/google/go-cmp/cmp"
	yamlv3 "gopkg.in/yaml.v3"
)

type YamlDocumentNodes struct {
	Nodes *[]*yamlv3.Node
	Raw   *[]byte
}

func get_key_value_in_map_node(keyName string, node *yamlv3.Node) (key, value *yamlv3.Node) {
	key = nil
	value = nil
	if node != nil {
		for i := 0; i < len(node.Content); i += 2 {
			currentNode := node.Content[i]
			if currentNode.Value == keyName {
				key = node.Content[i]
				value = node.Content[i+1]
				return
			}
		}
	}
	return
}

func get_index_in_slice(i int, node *yamlv3.Node) (result *yamlv3.Node) {
	result = nil
	if i > 0 {
		result = node.Content[i]
	}
	return
}

type YamlNodeDiff struct {
	Path []*yamlv3.Node
}

func (y *YamlNodeDiff) AddMapIndex(ps cmp.PathStep, node *yamlv3.Node) {

	r := reflect.ValueOf(ps)
	mi := r.Interface().(cmp.MapIndex)

	keyName := fmt.Sprintf("%s", mi.Key())
	key, value := get_key_value_in_map_node(keyName, node)

	y.Path = append(y.Path, key)
	y.Path = append(y.Path, value)
}

func (y *YamlNodeDiff) AddSliceIndex(ps cmp.PathStep, node *yamlv3.Node, A bool) {
	r := reflect.ValueOf(ps)
	si := r.Interface().(cmp.SliceIndex)

	Ai, Bi := si.SplitKeys()

	i := Ai
	if !A {
		i = Bi
	}

	y.Path = append(y.Path, get_index_in_slice(i, node))
}

func (y *YamlNodeDiff) IsNodeMappingType() (result bool) {
	result = false
	node := y.GetNode()
	if node != nil {
		result = node.Kind == yamlv3.MappingNode
	}
	return
}

func (y *YamlNodeDiff) GetNode() *yamlv3.Node {
	return y.Path[len(y.Path)-1]
}

func (y *YamlNodeDiff) GetParent() *yamlv3.Node {
	return y.Path[len(y.Path)-2]
}

func (y *YamlNodeDiff) GetLastNonEmpty() (result *yamlv3.Node) {
	for _, pn := range y.Path {
		if pn != nil {
			result = pn
		} else {
			return
		}
	}
	return
}

type NodeDiff struct {
	A *YamlDocumentNodes
	B *YamlDocumentNodes

	Ad []*YamlNodeDiff
	Bd []*YamlNodeDiff

	path cmp.Path

	index int
}

func (n *NodeDiff) getCurrentDocument() (An, Bn *yamlv3.Node) {
	return (*n.A.Nodes)[n.index], (*n.B.Nodes)[n.index]
}

func (n *NodeDiff) PushStep(pathstep cmp.PathStep) {
	n.path = append(n.path, pathstep)
}

func (n *NodeDiff) Report(result cmp.Result) {
	if !result.Equal() {

		A, B := n.getCurrentDocument()

		var An *yamlv3.Node
		if A != nil && len(A.Content) > 0 {
			An = A.Content[0]
		}
		Ay := YamlNodeDiff{
			Path: []*yamlv3.Node{A, An},
		}

		var Bn *yamlv3.Node
		if B != nil && len(B.Content) > 0 {
			Bn = B.Content[0]
		}
		By := YamlNodeDiff{
			Path: []*yamlv3.Node{B, Bn},
		}

		for _, ps := range n.path {
			switch ps.(type) {
			case cmp.MapIndex:
				Ay.AddMapIndex(ps, Ay.GetNode())
				By.AddMapIndex(ps, By.GetNode())
			case cmp.SliceIndex:
				Ay.AddSliceIndex(ps, Ay.GetNode(), true)
				By.AddSliceIndex(ps, By.GetNode(), true)
			}
		}

		n.Ad = append(n.Ad, &Ay)
		n.Bd = append(n.Bd, &By)
	}
}

func (n *NodeDiff) PopStep() {
	n.path = n.path[:len(n.path)-1]
}

func diff_document_nodes(index int, result *NodeDiff) (err error) {
	Am := map[interface{}]interface{}{}
	Bm := map[interface{}]interface{}{}

	var Ad *yamlv3.Node = (*result.A.Nodes)[index]
	if Ad != nil {
		if Ad.Kind == yamlv3.DocumentNode {
			err = Ad.Decode(&Am)
			if err != nil {
				return
			}
		} else {
			err = fmt.Errorf("nodes are of incorrect type (A: %v)", Ad.Kind)
			return
		}
	}

	var Bd *yamlv3.Node = (*result.B.Nodes)[index]
	if Bd != nil {
		if Bd.Kind == yamlv3.DocumentNode {
			//Bn = &B.Content
			err = Bd.Decode(&Bm)
			if err != nil {
				return
			}
		} else {
			err = fmt.Errorf("nodes are of incorrect type (B: %v)", Bd.Kind)
			return
		}
	}

	result.index = index
	cmp.Equal(Am, Bm, cmp.Reporter(result))

	return
}

func DiffDocumentNodes(A, B *YamlDocumentNodes) (result *NodeDiff, err error) {
	result = &NodeDiff{
		A:     A,
		B:     B,
		index: 0,
	}

	if len(*A.Nodes) == len(*B.Nodes) {
		for i := 0; i < len(*A.Nodes); i++ {
			err = diff_document_nodes(i, result)
			if err != nil {
				return
			}
		}
	}

	return
}

// func DiffYaml(Ab, Bb *[]byte) (nd *NodeDiff, err error) {
// 	Ay, err := NewYamlDocumentNodesFromBytes(Ab)
// 	if err != nil {
// 		return
// 	}
// 	By, err := NewYamlDocumentNodesFromBytes(Bb)
// 	if err != nil {
// 		return
// 	}

// 	return DiffDocumentNodes(Ay, By)
// }

// func DiffYamlReader(Ar, Br io.ReadSeeker) (nd *NodeDiff, err error) {
// 	Ay, err := NewYamlDocumentNodesFromReader(Ar)
// 	if err != nil {
// 		return
// 	}
// 	By, err := NewYamlDocumentNodesFromReader(Br)
// 	if err != nil {
// 		return
// 	}

// 	return DiffDocumentNodes(Ay, By)
// }

// func DiffDocumentNodes(Ay, By *YamlDocumentNodes) (nd *NodeDiff, err error) {
// 	nd = nil
// 	//organise_nodes(Ay.Nodes, By.Nodes)
// 	nd, err = diff_nodes(Ay, By)
// 	return
// }

func GetNodeLineRange(node *yamlv3.Node) (Start, End int) {
	Start = node.Line
	endNode := node
	End = -1

	for {
		if len(endNode.Content) > 0 {
			newNode := endNode
			for _, n := range endNode.Content {
				if n.Line > End {
					newNode = n
				}
			}
			if newNode != endNode {
				endNode = newNode
				End = endNode.Line
			} else {
				break
			}
		} else {
			End = endNode.Line
			break
		}
	}

	return
}
