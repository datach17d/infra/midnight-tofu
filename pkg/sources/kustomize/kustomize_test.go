package kustomize

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/datach17d/infra/midnight-tofu/pkg/yaml"
)

func TestGenerateNodesBasic(t *testing.T) {
	assert := assert.New(t)

	raw, nodes, err := GenerateNodes("testdata/basic_kustomize")
	assert.NoError(err)
	assert.Len(*nodes, 1)

	assert.NotNil(raw)
	assert.Greater(len(*raw), 0)
}

func TestBasicDiff(t *testing.T) {
	assert := assert.New(t)

	A, err := NewYamlDocumentNodesFromKustomizePath("testdata/basic_kustomize")
	assert.NoError(err)
	assert.NotNil(A)

	B, err := NewYamlDocumentNodesFromKustomizePath("testdata/basic_kustomize_patch")
	assert.NoError(err)
	assert.NotNil(B)

	nd, err := yaml.DiffDocumentNodes(A, B)
	assert.NoError(err)

	assert.Len(nd.Ad, 1)
	assert.Len(nd.Bd, 1)
}
