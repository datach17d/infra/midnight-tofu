package kustomize

import (
	"github.com/jinzhu/copier"
	"gitlab.com/datach17d/infra/midnight-tofu/pkg/utils"
	"gitlab.com/datach17d/infra/midnight-tofu/pkg/yaml"
	yamlv3 "gopkg.in/yaml.v3"
	"sigs.k8s.io/kustomize/api/krusty"
	"sigs.k8s.io/kustomize/kyaml/filesys"
)

func NewYamlDocumentNodesFromKustomizePath(path string) (result *yaml.YamlDocumentNodes, err error) {

	data, nodes, err := GenerateNodes(path)
	if err != nil {
		return
	}

	utils.SortYamlNodes(nodes)

	result = &yaml.YamlDocumentNodes{
		Nodes: nodes,
		Raw:   data,
	}

	return
}

func GenerateNodes(path string) (raw *[]byte, result *[]*yamlv3.Node, err error) {
	k := krusty.MakeKustomizer(krusty.MakeDefaultOptions())

	resmap, err := k.Run(filesys.MakeFsOnDisk(), path)
	if err != nil {
		return
	}

	rawData, err := resmap.AsYaml()
	if err != nil {
		return
	}
	raw = &rawData

	resources := resmap.Resources()
	data := make([]*yamlv3.Node, len(resources))
	result = &data
	for i, r := range resources {
		data[i] = &yamlv3.Node{}
		copier.Copy(data[i], r.RNode.Document())
	}

	return
}
