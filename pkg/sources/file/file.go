package file

import (
	"bytes"
	"io"
	"strings"

	"gitlab.com/datach17d/infra/midnight-tofu/pkg/utils"
	"gitlab.com/datach17d/infra/midnight-tofu/pkg/yaml"
	yamlv3 "gopkg.in/yaml.v3"
)

func get_nodes(in *[]byte, out *[]*yamlv3.Node) error {
	r := bytes.NewReader(*in)
	return get_nodes_using_reader(r, out)
}

func get_nodes_using_reader(r io.Reader, out *[]*yamlv3.Node) error {
	decoder := yamlv3.NewDecoder(r)
	for {
		var node yamlv3.Node
		if err := decoder.Decode(&node); err != nil {
			// Break when there are no more documents to decode
			if err != io.EOF {
				return err
			}
			break
		}
		*out = append(*out, &node)
	}
	return nil
}

func read_to_string_array(rs io.ReadSeeker) (result []string, err error) {
	rs.Seek(0, io.SeekStart)
	data, err := io.ReadAll(rs)

	if err == nil {
		result = append(result, strings.Split(string(data), "\n")...)
	}

	rs.Seek(0, io.SeekStart)
	return
}

func NewYamlDocumentNodesFromBytes(raw *[]byte) (result *yaml.YamlDocumentNodes, err error) {
	result = &yaml.YamlDocumentNodes{
		Nodes: &[]*yamlv3.Node{},
		Raw:   raw,
	}

	err = get_nodes(result.Raw, result.Nodes)

	return
}

func NewYamlDocumentNodesFromReader(r io.ReadSeeker) (result *yaml.YamlDocumentNodes, err error) {
	data, err := io.ReadAll(r)
	if err != nil {
		return
	}

	_, err = r.Seek(0, io.SeekStart)
	if err != nil {
		return
	}

	result = &yaml.YamlDocumentNodes{
		Nodes: &[]*yamlv3.Node{},
		Raw:   &data,
	}

	err = get_nodes_using_reader(r, result.Nodes)

	utils.SortYamlNodes(result.Nodes)

	return
}
