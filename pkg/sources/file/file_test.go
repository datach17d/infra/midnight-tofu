package file

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/datach17d/infra/midnight-tofu/pkg/utils"
	"gitlab.com/datach17d/infra/midnight-tofu/pkg/yaml"
	yamlv3 "gopkg.in/yaml.v3"
)

func TestDiffSimpleYaml(t *testing.T) {

	assert := assert.New(t)

	A := `
test1: test
test2:
  subtest21: 1
  subtest22: 2
test3:
  - 1
  - 2
test4:
   - test1: 1
   - test2: 2
`
	B := `
test1: test
test2:
  subtest21: 1
  subtest22: "2"
test3:
  - 1
  - "2"
test4:
  - test1: 1
  - test2: 2
  - test3: 3
`
	Ab := []byte(A)
	Bb := []byte(B)

	An, err := NewYamlDocumentNodesFromBytes(&Ab)
	assert.NoError(err)
	assert.Greater(len(*An.Nodes), 0)

	Bn, err := NewYamlDocumentNodesFromBytes(&Bb)
	assert.NoError(err)
	assert.Greater(len(*Bn.Nodes), 0)

	result, err := yaml.DiffDocumentNodes(An, Bn)
	assert.NoError(err)

	assert.Len(result.Ad, len(result.Bd))
	assert.Len(result.Bd, 3)
}

func TestNodeLines(t *testing.T) {
	assert := assert.New(t)

	A := `test1: one
test4:
  - 3
  - 4
  - 5
  - 6
test3:
  eight:
    nine:
      - ten`

	nodes := []*yamlv3.Node{}
	Ab := []byte(A)

	err := get_nodes(&Ab, &nodes)
	assert.NoError(err)

	start, end := yaml.GetNodeLineRange(nodes[0])
	assert.Equal(1, start)
	assert.Equal(10, end)

	start, end = yaml.GetNodeLineRange(nodes[0].Content[0].Content[3])
	assert.Equal(3, start)
	assert.Equal(6, end)

	start, end = yaml.GetNodeLineRange(nodes[0].Content[0].Content[5])
	assert.Equal(8, start)
	assert.Equal(10, end)
}

func TestKubernetesMutliDocumentAdd(t *testing.T) {
	assert := assert.New(t)

	A := `apiVersion: v1
kind: Secret
metadata:
  name: test1`

	B := `apiVersion: v1
kind: Secret
metadata: 
  name: test1
---
apiVersion: v1
kind: Secret
metadata:
  name: test2`

	An := []*yamlv3.Node{}
	Ab := []byte(A)
	err := get_nodes(&Ab, &An)
	assert.NoError(err)

	Bn := []*yamlv3.Node{}
	Bb := []byte(B)
	err = get_nodes(&Bb, &Bn)
	assert.NoError(err)

	assert.True(utils.CompareKubernetesNodes(An[0], Bn[0]))

	utils.OrganiseKubernetesNodes(&An, &Bn)

	assert.True(utils.CompareKubernetesNodes(An[0], Bn[0]))
	var nilNode *yamlv3.Node = nil

	assert.Equal(nilNode, An[1])
}

func TestKubernetesMutliDocumentRemove(t *testing.T) {
	assert := assert.New(t)

	A := `apiVersion: v1
kind: Secret
metadata: 
  name: test1
---
apiVersion: v1
kind: Secret
metadata:
  name: test2`

	B := `apiVersion: v1
kind: Secret
metadata:
  name: test1`

	An := []*yamlv3.Node{}
	Ab := []byte(A)
	err := get_nodes(&Ab, &An)
	assert.NoError(err)

	Bn := []*yamlv3.Node{}
	Bb := []byte(B)
	err = get_nodes(&Bb, &Bn)
	assert.NoError(err)

	assert.True(utils.CompareKubernetesNodes(An[0], Bn[0]))

	utils.OrganiseKubernetesNodes(&An, &Bn)

	assert.True(utils.CompareKubernetesNodes(An[0], Bn[0]))
	var nilNode *yamlv3.Node = nil

	assert.Equal(nilNode, Bn[1])
}

func TestKubernetesMutliDocumentAlignmentOrderMismatch(t *testing.T) {
	assert := assert.New(t)

	A := `apiVersion: v1
kind: Secret
metadata:
  name: test1
  namepsace: test1
---
apiVersion: v1
kind: Secret
metadata:
  name: test2
  labels:
    label1: "1"
---
apiVersion: v1
kind: Secret
metadata:
  name: test3
  namespace: test3
spec:
  data1: 1`

	B := `apiVersion: v1
kind: Secret
metadata:
  name: test1
  namepsace: test1
---
apiVersion: v1
kind: Secret
metadata:
  name: test3
  namespace: test3
---
apiVersion: v1
kind: Secret
metadata:
  name: test2`

	An := []*yamlv3.Node{}
	Ab := []byte(A)
	err := get_nodes(&Ab, &An)
	assert.NoError(err)

	Bn := []*yamlv3.Node{}
	Bb := []byte(B)
	err = get_nodes(&Bb, &Bn)
	assert.NoError(err)

	assert.True(utils.CompareKubernetesNodes(An[0], Bn[0]))
	assert.False(utils.CompareKubernetesNodes(An[1], Bn[1]))
	assert.False(utils.CompareKubernetesNodes(An[2], Bn[2]))

	assert.True(utils.CompareKubernetesNodes(An[1], Bn[2]))
	assert.True(utils.CompareKubernetesNodes(An[2], Bn[1]))

	utils.OrganiseKubernetesNodes(&An, &Bn)

	assert.True(utils.CompareKubernetesNodes(An[0], Bn[0]))
	assert.True(utils.CompareKubernetesNodes(An[1], Bn[1]))
	assert.True(utils.CompareKubernetesNodes(An[2], Bn[2]))
}

func TestKubernetesMutliDocumentMissing(t *testing.T) {
	assert := assert.New(t)

	A := `apiVersion: v1
kind: Secret
metadata:
  name: test1
  namepsace: test1
---
apiVersion: v1
kind: Secret
metadata:
  name: test2
  labels:
    label1: "1"
---
apiVersion: v1
kind: Secret
metadata:
  name: test5
  namespace: test5
spec:
  data1: 1`

	B := `apiVersion: v1
kind: Secret
metadata:
  name: test1
  namepsace: test1
---
apiVersion: v1
kind: Secret
metadata:
  name: test3
  namespace: test3
---
apiVersion: v1
kind: Secret
metadata:
  name: test4
---
apiVersion: v1
kind: Secret
metadata:
  name: test5
  namespace: test5`

	An := []*yamlv3.Node{}
	Ab := []byte(A)
	err := get_nodes(&Ab, &An)
	assert.NoError(err)

	Bn := []*yamlv3.Node{}
	Bb := []byte(B)
	err = get_nodes(&Bb, &Bn)
	assert.NoError(err)

	assert.True(utils.CompareKubernetesNodes(An[0], Bn[0]))
	assert.False(utils.CompareKubernetesNodes(An[1], Bn[1]))
	assert.False(utils.CompareKubernetesNodes(An[2], Bn[2]))

	utils.OrganiseKubernetesNodes(&An, &Bn)

	var nilNode *yamlv3.Node = nil

	assert.True(utils.CompareKubernetesNodes(An[0], Bn[0]))
	assert.Equal(nilNode, Bn[1])
	assert.Equal(nilNode, An[2])
	assert.Equal(nilNode, An[3])
	assert.True(utils.CompareKubernetesNodes(An[4], Bn[4]))

	assert.Equal(5, len(An))
	assert.Equal(len(An), len(Bn))
}
