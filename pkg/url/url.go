package url

import (
	"fmt"
	"net/url"
	"strings"
)

type SourceType int

const (
	Unkown SourceType = iota
	YamlSource
	KustomizeSource
)

var schemeMap map[string]SourceType = map[string]SourceType{
	"":                Unkown,
	"file":            YamlSource,
	"kustomize":       KustomizeSource,
	"kustomize+git":   KustomizeSource,
	"kustomize+https": KustomizeSource,
}

type YamlSourceInfo struct {
	Type SourceType
	Url  *url.URL
}

func ParseYamlSource(source string) (info YamlSourceInfo, err error) {

	u, err := url.Parse(source)

	if err != nil {
		//Try parse again if port error on git:
		if strings.HasPrefix(source, "kustomize+git") {
			i := strings.LastIndex(source, ":")
			u, err = url.Parse(source[:i+1] + "0/" + source[i+1:])
			if err != nil {
				return
			}

			p := strings.TrimLeft(u.Path, "/")
			j := strings.Index(p, "/")
			u.Path = p[j:]
			u.Host = u.Host[:strings.LastIndex(u.Host, ":")+1] + p[:j]
		} else {
			return
		}
	}

	var found bool
	if info.Type, found = schemeMap[u.Scheme]; !found {
		err = fmt.Errorf("scheme not supported %s", u.Scheme)
	}

	info.Url = u

	return
}
