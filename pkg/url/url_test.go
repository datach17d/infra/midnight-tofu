package url

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestParseYamlSource(t *testing.T) {
	assert := assert.New(t)

	s, err := ParseYamlSource("testing/test")
	assert.NoError(err)
	assert.Equal(Unkown, s.Type)

	s, err = ParseYamlSource("kustomize+git://git@gitlab.com:datach17d/infra/day2.git/apps/overlays")
	assert.NoError(err)
	assert.Equal(KustomizeSource, s.Type)

	s, err = ParseYamlSource("kustomize+git://gitlab.com/datach17d/infra/day2.git/apps/overlays")
	assert.NoError(err)
	assert.Equal(KustomizeSource, s.Type)

}
