package diff

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/datach17d/infra/midnight-tofu/pkg/sources/file"
	"gitlab.com/datach17d/infra/midnight-tofu/pkg/utils"
	"gitlab.com/datach17d/infra/midnight-tofu/pkg/yaml"
)

func TestGenerateDiffSimple(t *testing.T) {

	assert := assert.New(t)

	A := `test1: test
---
test1: 1`

	B := `test1: ntest
---
test1: 2`

	diff := `-test1: test
+test1: ntest
 ---
-test1: 1
+test1: 2`

	Ab := []byte(A)
	Bb := []byte(B)

	An, err := file.NewYamlDocumentNodesFromBytes(&Ab)
	assert.NoError(err)
	assert.Greater(len(*An.Nodes), 0)

	Bn, err := file.NewYamlDocumentNodesFromBytes(&Bb)
	assert.NoError(err)
	assert.Greater(len(*Bn.Nodes), 0)

	result, err := yaml.DiffDocumentNodes(An, Bn)
	assert.NoError(err)

	assert.Len(result.Ad, len(result.Bd))
	assert.Len(result.Bd, 2)

	assert.Equal(diff, GenerateDiff(result))
}

func TestGenerateDiffTypeChangeStringToMap(t *testing.T) {

	assert := assert.New(t)

	A := `test1: test`

	B := `test1:
  test2: test`

	diff := `-test1: test
+test1:
+  test2: test`

	Ab := []byte(A)
	Bb := []byte(B)

	An, err := file.NewYamlDocumentNodesFromBytes(&Ab)
	assert.NoError(err)
	assert.Greater(len(*An.Nodes), 0)

	Bn, err := file.NewYamlDocumentNodesFromBytes(&Bb)
	assert.NoError(err)
	assert.Greater(len(*Bn.Nodes), 0)

	result, err := yaml.DiffDocumentNodes(An, Bn)
	assert.NoError(err)

	assert.Len(result.Ad, len(result.Bd))
	assert.Len(result.Bd, 1)

	assert.Equal(diff, GenerateDiff(result))
}

func TestGenerateDiffTypeChangeStringToList(t *testing.T) {

	assert := assert.New(t)

	A := `test1: test`

	B := `test1:
  - 1
  - 2`

	diff := `-test1: test
+test1:
+  - 1
+  - 2`

	Ab := []byte(A)
	Bb := []byte(B)

	An, err := file.NewYamlDocumentNodesFromBytes(&Ab)
	assert.NoError(err)
	assert.Greater(len(*An.Nodes), 0)

	Bn, err := file.NewYamlDocumentNodesFromBytes(&Bb)
	assert.NoError(err)
	assert.Greater(len(*Bn.Nodes), 0)

	result, err := yaml.DiffDocumentNodes(An, Bn)
	assert.NoError(err)

	assert.Len(result.Ad, len(result.Bd))
	assert.Len(result.Bd, 1)

	assert.Equal(diff, GenerateDiff(result))
}

func TestGenerateDiffRemoveAddMapMembers(t *testing.T) {

	assert := assert.New(t)

	A := `test1: test
test2:
  test1: one
  test2: two
  test3:
    test1: one
    test2: two`

	B := `test1: test
test2:
  test1: one
  test3:
    test2: two`

	diff := ` test1: test
 test2:
   test1: one
-  test2: two
   test3:
-    test1: one
     test2: two`

	diffReverse := ` test1: test
 test2:
   test1: one
+  test2: two
   test3:
     test2: two
+    test1: one`

	Ab := []byte(A)
	Bb := []byte(B)

	An, err := file.NewYamlDocumentNodesFromBytes(&Ab)
	assert.NoError(err)
	assert.Greater(len(*An.Nodes), 0)

	Bn, err := file.NewYamlDocumentNodesFromBytes(&Bb)
	assert.NoError(err)
	assert.Greater(len(*Bn.Nodes), 0)

	result, err := yaml.DiffDocumentNodes(An, Bn)
	assert.NoError(err)

	assert.Len(result.Ad, len(result.Bd))
	assert.Len(result.Bd, 2)

	assert.Equal(diff, GenerateDiff(result))

	result, err = yaml.DiffDocumentNodes(Bn, An)
	assert.NoError(err)

	assert.Len(result.Ad, len(result.Bd))
	assert.Len(result.Bd, 2)

	assert.Equal(diffReverse, GenerateDiff(result))
}

func TestGenerateDiffSpaceBetweenObjects(t *testing.T) {

	assert := assert.New(t)

	A := `test1: test
test2: test`

	B := `test1: test

test2: notest`

	diff := ` test1: test
-test2: test
+test2: notest`

	Ab := []byte(A)
	Bb := []byte(B)

	An, err := file.NewYamlDocumentNodesFromBytes(&Ab)
	assert.NoError(err)
	assert.Greater(len(*An.Nodes), 0)

	Bn, err := file.NewYamlDocumentNodesFromBytes(&Bb)
	assert.NoError(err)
	assert.Greater(len(*Bn.Nodes), 0)

	result, err := yaml.DiffDocumentNodes(An, Bn)
	assert.NoError(err)

	assert.Len(result.Ad, len(result.Bd))
	assert.Len(result.Bd, 1)

	assert.Equal(diff, GenerateDiff(result))
}

func TestGenerateDiffMissingRootItem(t *testing.T) {

	assert := assert.New(t)

	A := `test1: test
test2: test`

	B := `test1: test`

	diff := ` test1: test
-test2: test`

	Ab := []byte(A)
	Bb := []byte(B)

	An, err := file.NewYamlDocumentNodesFromBytes(&Ab)
	assert.NoError(err)
	assert.Greater(len(*An.Nodes), 0)

	Bn, err := file.NewYamlDocumentNodesFromBytes(&Bb)
	assert.NoError(err)
	assert.Greater(len(*Bn.Nodes), 0)

	result, err := yaml.DiffDocumentNodes(An, Bn)
	assert.NoError(err)

	assert.Len(result.Ad, len(result.Bd))
	assert.Len(result.Bd, 1)

	assert.Equal(diff, GenerateDiff(result))
}

func TestGenerateDiffMissingRootMap(t *testing.T) {

	assert := assert.New(t)

	A := `test1: test
test2:
  test3: test`

	B := `test1: test`

	diff := ` test1: test
-test2:
-  test3: test`

	Ab := []byte(A)
	Bb := []byte(B)

	An, err := file.NewYamlDocumentNodesFromBytes(&Ab)
	assert.NoError(err)
	assert.Greater(len(*An.Nodes), 0)

	Bn, err := file.NewYamlDocumentNodesFromBytes(&Bb)
	assert.NoError(err)
	assert.Greater(len(*Bn.Nodes), 0)

	result, err := yaml.DiffDocumentNodes(An, Bn)
	assert.NoError(err)

	assert.Len(result.Ad, len(result.Bd))
	assert.Len(result.Bd, 1)

	assert.Equal(diff, GenerateDiff(result))
}

func TestGenerateDiffNewRootItem(t *testing.T) {

	assert := assert.New(t)

	A := `test1: test
---
test3: 3`

	B := `test1: test
test2: test
---
test3: 3`

	diff := ` test1: test
+test2: test
 ---
 test3: 3`

	Ab := []byte(A)
	Bb := []byte(B)

	An, err := file.NewYamlDocumentNodesFromBytes(&Ab)
	assert.NoError(err)
	assert.Greater(len(*An.Nodes), 0)

	Bn, err := file.NewYamlDocumentNodesFromBytes(&Bb)
	assert.NoError(err)
	assert.Greater(len(*Bn.Nodes), 0)

	result, err := yaml.DiffDocumentNodes(An, Bn)
	assert.NoError(err)

	assert.Len(result.Ad, len(result.Bd))
	assert.Len(result.Bd, 1)

	assert.Equal(diff, GenerateDiff(result))
}

func TestGenerateDiffNewRootMap(t *testing.T) {

	assert := assert.New(t)

	A := `test1: test`

	B := `test1: test
test2:
  test3: test`

	diff := ` test1: test
+test2:
+  test3: test`

	Ab := []byte(A)
	Bb := []byte(B)

	An, err := file.NewYamlDocumentNodesFromBytes(&Ab)
	assert.NoError(err)
	assert.Greater(len(*An.Nodes), 0)

	Bn, err := file.NewYamlDocumentNodesFromBytes(&Bb)
	assert.NoError(err)
	assert.Greater(len(*Bn.Nodes), 0)

	result, err := yaml.DiffDocumentNodes(An, Bn)
	assert.NoError(err)

	assert.Len(result.Ad, len(result.Bd))
	assert.Len(result.Bd, 1)

	assert.Equal(diff, GenerateDiff(result))
}

func TestGenerateDiffK8sAddRemoveResourceEnd(t *testing.T) {

	assert := assert.New(t)

	A := `apiVersion: v1
kind: Secret
metadata:
  name: test1`

	B := `apiVersion: v1
kind: Secret
metadata:
  name: test1
---
apiVersion: v1
kind: Secret
metadata:
  name: test2`

	diffAdd := ` apiVersion: v1
 kind: Secret
 metadata:
   name: test1
 ---
+apiVersion: v1
+kind: Secret
+metadata:
+  name: test2`

	diffRemove := ` apiVersion: v1
 kind: Secret
 metadata:
   name: test1
 ---
-apiVersion: v1
-kind: Secret
-metadata:
-  name: test2`

	Ab := []byte(A)
	Bb := []byte(B)

	An, err := file.NewYamlDocumentNodesFromBytes(&Ab)
	assert.NoError(err)
	assert.Greater(len(*An.Nodes), 0)

	Bn, err := file.NewYamlDocumentNodesFromBytes(&Bb)
	assert.NoError(err)
	assert.Greater(len(*Bn.Nodes), 0)

	utils.LineUpNodes(An.Nodes, Bn.Nodes)

	result, err := yaml.DiffDocumentNodes(An, Bn)
	assert.NoError(err)

	assert.Len(result.Ad, len(result.Bd))
	assert.Len(result.Bd, 3)

	assert.Equal(diffAdd, GenerateDiff(result))

	result, err = yaml.DiffDocumentNodes(Bn, An)
	assert.NoError(err)

	assert.Len(result.Ad, len(result.Bd))
	assert.Len(result.Bd, 3)

	assert.Equal(diffRemove, GenerateDiff(result))
}

func aTestGenerateDiffK8sAddRemoveResourceBegin(t *testing.T) {

	assert := assert.New(t)

	A := `apiVersion: v1
kind: Secret
metadata:
  name: test2`

	B := `apiVersion: v1
kind: Secret
metadata:
  name: test1
---
apiVersion: v1
kind: Secret
metadata:
  name: test2`

	diffAdd := `+apiVersion: v1
+kind: Secret
+metadata:
+  name: test1
 ---
 apiVersion: v1
 kind: Secret
 metadata:
   name: test2`

	diffRemove := `-apiVersion: v1
-kind: Secret
-metadata:
-  name: test1
 ---
 apiVersion: v1
 kind: Secret
 metadata:
   name: test2`

	Ab := []byte(A)
	Bb := []byte(B)

	An, err := file.NewYamlDocumentNodesFromBytes(&Ab)
	assert.NoError(err)
	assert.Greater(len(*An.Nodes), 0)

	Bn, err := file.NewYamlDocumentNodesFromBytes(&Bb)
	assert.NoError(err)
	assert.Greater(len(*Bn.Nodes), 0)

	result, err := yaml.DiffDocumentNodes(An, Bn)
	assert.NoError(err)

	assert.Len(result.Ad, len(result.Bd))
	assert.Len(result.Bd, 3)

	assert.Equal(diffAdd, GenerateDiff(result))

	result, err = yaml.DiffDocumentNodes(Bn, An)
	assert.NoError(err)

	assert.Len(result.Ad, len(result.Bd))
	assert.Len(result.Bd, 3)

	assert.Equal(diffRemove, GenerateDiff(result))
}
