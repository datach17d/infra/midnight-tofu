package diff

import (
	"strings"

	"gitlab.com/datach17d/infra/midnight-tofu/pkg/yaml"
)

func set_prefix(prefix rune, str string) (result string) {
	rstr := []rune(str)
	rstr[0] = prefix
	result = string(rstr)
	return
}

func GenerateDiff(nd *yaml.NodeDiff) (output string) {

	Araw := strings.Split(string(*nd.A.Raw), "\n")
	Braw := strings.Split(string(*nd.B.Raw), "\n")

	for i := range Araw {
		Araw[i] = " " + Araw[i]
	}

	for i, Ay := range nd.Ad {
		By := nd.Bd[i]
		An := Ay.GetNode()
		Bn := By.GetNode()
		var aEnd int
		var start int
		var bEnd int
		if An != nil {
			start, aEnd = yaml.GetNodeLineRange(An)

			if Ay.IsNodeMappingType() {
				Ayp := Ay.GetParent()
				start, _ = yaml.GetNodeLineRange(Ayp)
			}

			for i := start; i <= aEnd; i++ {
				Araw[i-1] = set_prefix('-', Araw[i-1])
			}
		}

		if Bn != nil {
			start, bEnd = yaml.GetNodeLineRange(Bn)
			outputBadd := ""

			BnP := By.GetParent()

			if By.IsNodeMappingType() {
				start, _ = yaml.GetNodeLineRange(BnP)
			}

			if An != nil {
				if start != BnP.Line {
					outputBadd = "\n+" + Braw[BnP.Line-1]
				}
			}

			if An == nil {
				Anl := Ay.GetLastNonEmpty()
				if Anl != nil {
					aEnd = Anl.Line
				} else {
					aEnd = len(Araw)

					if Ay.Path[0] == nil {
						if start == 1 || (start == By.Path[0].Line+1 && start != 1 && By.Path[0].Line != 1) {
							outputBadd = outputBadd + "\n ---"
						}
					}
				}
			}

			for i := start; i <= bEnd; i++ {
				outputBadd = outputBadd + "\n+" + Braw[i-1]
			}
			Araw[aEnd-1] = Araw[aEnd-1] + outputBadd
		}
	}

	return strings.Join(Araw, "\n")
}
