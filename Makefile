all: vet test build

build:
	go build -o midnight-tofu ./cmd/midnight-tofu 

test:
	go test ./...

vet:
	go vet ./...